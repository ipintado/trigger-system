require('dotenv').config();
const pg = require('pg');
const Queue = require('bull');
const Hapi = require('hapi');
const Swagger = require('hapi-swagger');
const Inert = require('inert');
const Vision = require('vision');
const util = require('util');
const Joi = require('joi');

const postgresConfig = {
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT
};

const knex = require('knex')({
  client: 'postgres',
  connection: postgresConfig
});
const client = new pg.Client(postgresConfig);
const eventsQueue = new Queue('events');
const routes = [{
  method: 'POST',
  path: '/record',
  options: {
    tags: ['api'],
    validate: {
      payload: {
        user_id: Joi.number().default(0),
        field1: Joi.string().default(''),
        field2: Joi.string().default(''),
        value1: Joi.number().default(0)
      }
    }
  },
  handler: async (req) => {
    await createRecord(req.payload);
    return {};
  }
}];

async function createRecord (recordData) {
  return knex.transaction(async transaction => {
    await transaction.table('record').insert(recordData);
    const ids = await transaction.table('events').insert({ event_type: 1, data: recordData }).returning('id');
    const recordId = ids[0];
    console.log(`Created new record with id ${recordId}`);
  });
}

async function runServer () {
  const server = new Hapi.Server({ port: 3001 });
  await server.register([Vision, Inert, Swagger]);
  server.route(routes);
  await server.start();
  console.log(server.info);
}

async function findActions (triggers) {
  const ids = triggers.map(t => t.id);
  return knex.table('actions').select()
    .innerJoin('trigger_actions', 'actions.id', 'trigger_actions.action_id')
    .whereIn('trigger_actions.trigger_id', ids);
}

function isConditionValid (condition, value) {
  switch (condition.operator) {
    case 'gte': {
      return value >= condition.value;
    }
    case 'lte': {
      return value <= condition.value;
    }
    case 'eq': {
      return value === condition.value;
    }
    case 'ne': {
      return value !== condition.value;
    }
  }
}

async function processEvents ({ data }) {
  try {
    console.log(`Event being processed in queue with data: ${util.inspect(data)}`);
    const triggers = await knex.table('triggers').select('id').where('event_type', data.event_type);
    const actions = await findActions(triggers);
    const validActions = actions.filter(action => data.data.hasOwnProperty(action.field));
    for (const action of validActions) {
      const condition = action.conditions;
      const fieldValue = data.data[action.field];
      if (isConditionValid(condition, fieldValue)) {
        console.log(`Action: ${action.name} triggered because ${action.field} with 
        value:${fieldValue} satisfies the following conditions ${util.inspect(action.conditions)}`);
      } else {
        console.log(`Action: ${action.name} NOT triggered because ${action.field} with 
        value:${fieldValue} does not satisfy the following conditions: ${util.inspect(action.conditions)}`);
      }
    }
    return Promise.resolve();
  } catch (e) {
    console.log(e);
  }
}

(async () => {
  await client.connect();
  await client.query('LISTEN event');
  client.on('notification', async (message) => {
    const eventRow = JSON.parse(message.payload);
    console.log(`Added new event to queue with data: ${util.inspect(eventRow)}`);
    eventsQueue.add('event', eventRow);
  });
  eventsQueue.process('event', processEvents);
  await runServer();
})();
