
exports.up = function (knex) {
  return knex.schema.createTable('events_types', e => {
    e.increments('id');
    e.timestamps(true, true);
    e.string('name');
  })
    .createTable('events', t => {
      t.increments('id');
      t.timestamps(true, true);
      t.json('data');
      t.integer('event_type').references('id').inTable('events_types').onDelete('CASCADE');
    })
    .createTable('triggers', t => {
      t.increments('id');
      t.timestamps(true, true);
      t.string('name');
      t.string('description');
      t.integer('event_type').references('id').inTable('events_types').onDelete('CASCADE');
    })
    .createTable('actions', a => {
      a.increments('id');
      a.timestamps(true, true);
      a.string('name');
      a.string('description');
      a.string('field'); // Should this be a foreig key to a table with a defined list of fields
    })
    .createTable('trigger_actions', ta => {
      ta.increments('id');
      ta.integer('action_id').references('id').inTable('actions').onDelete('CASCADE');
      ta.integer('trigger_id').references('id').inTable('triggers').onDelete('CASCADE');
      ta.integer('priority');
      ta.json('conditions'); // Need to define structure for this
    })
    .createTable('record', r => {
      r.increments('id');
      r.timestamps(true, true);
      r.integer('user_id');
      r.string('field1');
      r.integer('value1');
      r.string('field2');
    });
};

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('record')
    .dropTable('trigger_actions')
    .dropTable('events')
    .dropTable('triggers')
    .dropTable('events_types')
    .dropTable('actions');
};
