
exports.up = function (knex) {
  return knex.schema.raw(`
  CREATE OR REPLACE FUNCTION public.notify_new_event()
    RETURNS trigger
    LANGUAGE plpgsql
    AS $function$
     BEGIN
         PERFORM pg_notify('event', row_to_json(NEW)::text);
         RETURN NULL;
     END; 
    $function$;`)
    .raw(`
      CREATE TRIGGER new_event
        AFTER insert or update
        ON "events"
        FOR EACH ROW
        EXECUTE PROCEDURE notify_new_event();`
    );
};

exports.down = function (knex) {
  return knex.schema.raw(`DROP TRIGGER IF EXISTS new_event on events;`)
    .raw(`DROP FUNCTION IF EXISTS notify_new_event();`);
};
